<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\LocationsController;
use App\Http\Controllers\API\MetadataController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\API\AssetController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('jwt.auth')->group(function () {
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::get('/user', [AuthController::class, 'user_profile']);
    Route::controller(MetadataController::class)->group(function () {
        Route::get('departments',  'getDepartments');
        Route::get('countries', 'getCountries');
        Route::get('categories', 'getCategories');
        Route::get('manufacturers', 'getManufacturers');
        Route::get('models','getModels');
        Route::get('suppliers', 'getSuppliers');
    });
    Route::prefix('dashboard')->controller(DashboardController::class)->group(function (){
        Route::get('/total-users','getTotalUsers')->name('total_user');
        Route::get('/total-assets','getTotalAssets');
        Route::get('/recent-users','getRecentUsers');
        Route::get('/recent-assets','getRecentAssets');
    });
    Route::prefix('locations')->controller(LocationsController::class)->group(function () {
        Route::get('/', 'index')->name('location.home');
        Route::post('/search', 'search');
        Route::post('/', 'store');
        Route::post('/copy/{id}', 'copy');
        Route::post('/import', 'import');
        Route::get('/{id}', 'show');
        Route::put('/{id}', 'update');
        Route::delete('/{id}', 'destroy');
    });

    Route::prefix('asset')->controller(AssetController::class)->group(function (){
        Route::get('/', 'index')->name('asset.home');
        Route::post('/search', 'search');
        Route::post('/', 'store');
        Route::put('/{id}', 'update');
        Route::delete('/{id}', 'destroy');
        Route::get('qr-code/{id}',[AssetController::class,'QRcode']);
        Route::post('/import','import');

    });
    Route::post('/model/choose-model/{id}',[\App\Http\Controllers\ManufactoryController::class,'choose_model'])->name('location.chooseAddress');

});
Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login')->name('loginAPI');
    Route::post('register', 'register');
    Route::post('verify-email', 'verifyEmail');


});




