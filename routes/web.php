<?php

use App\Http\Controllers\EmailController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Models\Manufactory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/




Route::group(['middleware' => 'guest'], function () {
    Route::get('/log-in', [LoginController::class,'index'])->name('login');
    Route::post('/log-in', [LoginController::class,'login'])->name('postlogin');

    Route::view('/register', 'auth.register')->name('register');
    Route::post('register',[RegisterController::class,'store'])->name('postRegister');

    Route::get('/verify-email/{email}',[EmailController::class,'index'])->name('verify');
    Route::post('/verify-email/{email}',[EmailController::class,'store'])->name('postVerify');
    Route::post('/resendToken/{email}',[EmailController::class,'resendToken'])->name('resendToken');


    Route::get('/forgot-password',[\App\Http\Controllers\Auth\ForgotPasswordController::class,'index'] )->name('password.request');
    Route::post('/forgot-password', [\App\Http\Controllers\Auth\ForgotPasswordController::class,'password_send'])->name('password.email');
    Route::get('/reset-password/{token}', [\App\Http\Controllers\Auth\ResetPasswordController::class,'showResetPasswordForm'])->name('password.reset');
    Route::post('/reset-password', [\App\Http\Controllers\Auth\ResetPasswordController::class,'reset'])->name('password.update');

});

Route::view('/','welcome');

Route::middleware('auth')->group(function () {
    Route::get('/home',[HomeController::class,'index'])->name('home');
    Route::post('/log-out', [LoginController::class, 'logout'])->name('logout');
    Route::prefix('locations')->group(function (){
        Route::get('/',[\App\Http\Controllers\LocationController::class,'index'])->name('locations');
        Route::get('/get/{id}', [\App\Http\Controllers\LocationController::class, 'show'])->name('copyLocation');
        Route::get('/copy/{id}', [\App\Http\Controllers\LocationController::class, 'copy'])->name('copyLocation');
        Route::post('/create',[\App\Http\Controllers\LocationController::class,'store'])->name('createLocation');
        Route::post('/edit/{id}', [\App\Http\Controllers\LocationController::class, 'edit'])->name('editLocation');
        Route::post('/delete/{id}', [\App\Http\Controllers\LocationController::class, 'destroy'])->name('deleteLocation');
        Route::post('/import-data', [\App\Http\Controllers\LocationController::class, 'importData'])->name('importData');
        Route::get('/upload_file', [\App\Http\Controllers\LocationController::class, 'upload_file'])->name('upload_file');
        Route::post('/new-location', [\App\Http\Controllers\LocationController::class, 'newLocation'])->name('newLocation');
    });
    Route::prefix('role-user')->group(function (){
    });
    Route::prefix('asset')->group(function (){
        Route::get('/',function (){
            $conditions = \App\Models\Condition::all();
            $categories = \App\Models\Categories::all();
            $manufactories = Manufactory::all();
            $locations = \App\Models\Locations::all();
            $suppliers =  \App\Models\Suppliers::all();
            $countries = \App\Models\app_countries::all();
            return view('asset',compact('conditions','countries','categories', 'manufactories','locations','suppliers'));
        });
    });
});

