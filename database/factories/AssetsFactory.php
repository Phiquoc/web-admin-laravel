<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Assets>
 */
class AssetsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->word(),
            'condition' => $this->faker->word(),
            'note' => $this->faker->word(),
            'asset_code' => implode(' ', $this->faker->unique()->words(2)),
            'category_id' => function () {
                return DB::table('categories')->inRandomOrder()->first()->id;
            },
            'country_id' => function () {
                return DB::table('apps_countries')->inRandomOrder()->first()->id;
            },
            'location_id' => function () {
                return DB::table('locations')->inRandomOrder()->first()->id;
            },
        ];
    }
}
