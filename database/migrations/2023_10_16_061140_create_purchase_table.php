<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('purchase', function (Blueprint $table) {
            $table->id();
            $table->string('serial');
            $table->date('date');
            $table->float('price');
            $table->unsignedBigInteger('supplier_id');
            $table->unsignedBigInteger('manufactory_id');
            $table->unsignedBigInteger('model_id');
            $table->foreign('supplier_id')->references('id')->on('supplies')->onDelete('cascade');
            $table->foreign('manufactory_id')->references('id')->on('manufactories')->onDelete('cascade');
          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('purchase');
    }
};
