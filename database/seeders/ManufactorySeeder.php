<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ManufactorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data=[
            ['name'=>'Panasonic'],
            ['name' => 'Samsung'],
            ['name' => 'Sony'],
            ['name' => 'LG'],
            ['name' => 'Apple'],
            ['name' => 'Nike'],
            ['name' => 'Microsoft'],
            ['name' => 'Coca-cola'],
            ['name' => 'Toyota'],
            ['name' => 'Yamaha'],
            ['name' => 'Dell'],
            ['name' => 'Asus'],
        ];
        DB::table('manufactories')->insert($data);
    }
}
