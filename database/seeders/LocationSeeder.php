<?php

namespace Database\Seeders;


use App\Models\Locations;
use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Locations::factory()->count(100)->create();

    }
}
