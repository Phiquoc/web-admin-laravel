<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            ['name'=> 'IT Department'],
            ['name'=> 'HR Department'],
            ['name'=> 'Marketing Department'],
            ['name'=> 'Buying Department'],
            ['name'=> 'Sales Department'],
            ['name'=> 'Acounting Department'],
            ['name'=> 'Design Department'],
          
        ];
        DB::table('department')->insert($data);
    }
}
