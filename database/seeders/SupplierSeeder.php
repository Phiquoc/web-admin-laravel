<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data=[
            ['name'=>'Phong Vu computer LTD'],
            ['name'=>'Tin hoc ngoi sao computer LTD'],
            ['name'=>'GearVN computer LTD'],
            ['name'=>'Cellphones computer LTD'],
            ['name'=>'TGDD computer LTD'],
            ['name'=>'Hoang Ha computer LTD'],
            ['name'=>'Quang Anh computer LTD'],
            ['name'=>'BaO Đại computer LTD'],
            ['name'=>'Quốc Thống computer LTD'],
            ['name'=>'F88 computer LTD'],

        ];
        DB::table('supplies')->insert($data);
    }
}
