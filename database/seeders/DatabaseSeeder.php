<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Location;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            CountrySeeder::class,
            ManufactorySeeder::class,
            ModelsSeeder::class,
            CategorySeeder::class,
            DepartmentSeeder::class,
            SupplierSeeder::class,
            RoleSeeder::class,
            LocationSeeder::class,
            AssetSeeder::class,
            UserSeeder::class
        ]);
    }
}
