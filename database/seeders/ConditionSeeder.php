<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConditionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data=[
            ['name'=>'NON EXISTENT','description'=>'asset abandoned or no long exists '],
            ['name'=>'VERY GOOD','description'=>'asset abandoned or no long exists '],
            ['name'=>'GOOD','description'=>'asset abandoned or no long exists '],
            ['name'=>'FAIR','description'=>'asset abandoned or no long exists '],
            ['name'=>'REQUIRES RENEWAL','description'=>'asset abandoned or no long exists '],
            ['name'=>'UNSERVICEABLE','description'=>'asset abandoned or no long exists '],

        ];
        DB::table('condition')->insert($data);
    }
}
