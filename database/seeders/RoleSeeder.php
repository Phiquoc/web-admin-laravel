<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            ['name' => 'customer', 'description' => 'Buying evething'],
            ['name' => 'employee', 'description' => 'Manage product'],
            ['name' => 'Manager', 'description' => 'Manage employee'],
            ['name' => 'Director', 'description' => 'Develop bussiness'],
        ];
        DB::table('roles')->insert($data);
    }
}
