<?php

namespace Database\Seeders;

use App\Models\Assets;
use Illuminate\Database\Seeder;

class AssetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Assets::factory()->count(15)->create();
    }
}
