<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data=[
            ['name'=>'Cash and cash equivalents'],
            ['name'=>'Accounts Receivable'],
            ['name'=>'Inventory'],
            ['name'=>'Investments'],
            ['name'=>'PBE (Property, Plant, and Equipment)'],
            ['name'=>'Vehicles'],
            ['name'=>'Furniture'],
            ['name'=>'Patents (intangible asset)'],
            ['name'=>'Economic Value'],
            ['name'=>'Resource'],
        ];
        DB::table('categories')->insert($data);
    }
}
