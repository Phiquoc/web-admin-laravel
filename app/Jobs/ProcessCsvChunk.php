<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;

class ProcessCsvChunk implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $chucks;
    /**
     * Create a new job instance.
     */
    public function __construct($chucks)
    {
        $this->chucks = $chucks;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $chuck = $this->chucks;
        $chunks = array_chunk($chuck, 20);
        $path = storage_path('temp');
        foreach ($chunks as $key => $chunk) {
            $csv_file = $path . "/csv_chunk-{$key}-" . Str::random(20) . ".csv";
            file_put_contents($csv_file, $chunk);
        }
    }
}
