<?php

namespace App\Http\Requests\Location;

use Illuminate\Foundation\Http\FormRequest;

class UpdateLocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'string|min:3|max:255',
            'department_id' => 'numeric|exists:department,id',
            'note' => 'string|max:512'
        ];
    }
    public function param(){
        return[
            'name' => $this->name,
            'department_id' => $this->department_id,
            'note' =>$this->note
        ];
    }
}
