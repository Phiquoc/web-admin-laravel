<?php

namespace App\Http\Requests\Location;

use Illuminate\Foundation\Http\FormRequest;

class CreateLocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|min:3|max:255',
            'department_id'=>'required|numeric',
            'note' => 'required|string|min:10|max:512'
        ];
    }
    public function param(){
        return [
            'name' => $this->name,
            'note' =>$this->note,
            'department_id' =>$this->department_id,
        ];
    }
}
