<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAssetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'string|max:255',
            'condition' => 'string|max:255',
            'note' => 'string|max:255',
            'asset_code' => 'string|max:20|unique:assets,asset_code',
            'category_id' => 'integer|exists:categories,id',
            'country_id' => 'integer|exists:apps_countries,id',
            'location_id' => 'integer|exists:locations,id',
        ];
    }
}
