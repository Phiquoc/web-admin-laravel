<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateAssetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'condition_id' => 'required|integer|exists:conditions,id',
            'note' => 'string|max:255',
            'category_id' => 'required|integer|exists:categories,id',
            'country_id' => 'required|integer|exists:apps_countries,id',
            'location_id' => 'required|integer|exists:locations,id',
        ];
    }
    public function param(){
        return [
            'name' => $this->name,
            'condition_id' => $this->condition_id,
            'note' => $this->note,
            'category_id' => $this->category_id,
            'country_id' => $this->country_id,
            'location_id' => $this->location_id,
        ];
    }
}
