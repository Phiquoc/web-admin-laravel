<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Auth\Middleware\Authenticate;
class JWTMiddleware extends  Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    protected $auth;

    public function __construct(JWTAuth $auth)
    {
        $this->auth = $auth;
    }
    protected function redirectTo(Request $request): ?string
    {
        return $request->expectsJson() ? null : route('login');
    }
    public function handle($request, Closure $next ,...$guards)
    {
        if ($token = $request->cookie(config("jwt.cookie"))) {
            try {
                $user = $this->auth->parseToken($token)->authenticate();

                if ($user) {
                    auth()->setUser($user);
                }
            } catch (\Exception $e) {

            }
        }
        return $next($request);
    }
}
