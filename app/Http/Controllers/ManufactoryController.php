<?php

namespace App\Http\Controllers;

use App\Locations;
use App\Models\Manufactory;
use App\Models\Models;
use Illuminate\Http\Request;

class ManufactoryController extends Controller
{
    public function choose_model($id){
        $get_model= Models::where('manufactory_id',$id)->get();
        $data = '<option value="" >Please choose Model</option>';
        foreach($get_model as $key => $val){
            $data .= '<option value="'.$val->id.'"> '.$val->name.'</option>';
        }
        echo $data;
    }
}
