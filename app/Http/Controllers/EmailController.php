<?php

namespace App\Http\Controllers;

use App\Http\Requests\VerifyRequest;
use App\Jobs\EmailQueuesJob;
use App\Models\User;
use App\Repositories\Post\UserRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EmailController extends Controller
{
    protected $UserRepository;

    public function __construct(UserRepository $UserRepository)
    {
        $this->UserRepository = $UserRepository;
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }
    public function index($email)
    {

        return view('auth.verifiemail', ['email' => $email]);
    }
    public function store(VerifyRequest $request, $email)
    {
        $user = $this->UserRepository->findemail($email);
        $data = $request->all();
        $token = $data['token'];
        $now = Carbon::now();
        $expiresAt = Carbon::parse($user->verification_code_expires_at);
        $minutesDifference = $now->diffInMinutes($expiresAt);
        if ($minutesDifference > 5) {
            return  redirect()->back()->with(['error' => 'Over token timeout'], 400);
        }
        if ($user->id) {
            if ($token == $user['remember_token']) {
                $data['status'] = 'active';

                if ($this->UserRepository->update($user->id, $data)) {
                    return redirect()->route('login')->with('success','Login');
                } else {
                    redirect()->back()->with('error', 'Failed to update user status');
                }
            } else {
                return redirect()->back()->with('error', 'Invalid token or user not found');
            }
        }
    }
    public function resendToken($email)
    {
        $user = $this->UserRepository->findemail($email);
        $token = $user->remember_token = Str::random(6);
        if ($user) {

            $sendJob = (new EmailQueuesJob($user->name, $email, $token))->delay(Carbon::now()->addSeconds(5));
            dispatch($sendJob);

            if ($this->UserRepository->update($user->id, ['remember_token'=>$token,
                'verification_code_expires_at' => now()])) {
                return redirect()->back()->with('message', 'Token has been sent via email');
            }
        }
    }
}
