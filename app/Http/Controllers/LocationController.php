<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLocationRequest;
use App\Http\Requests\ImportDataRequest;
use App\Imports\LocationDataImport;
use App\Jobs\ImportQueuesJob;
use App\Jobs\ProcessCsvChunk;
use App\Models\Locations;
use Exception;
use App\Repositories\Post\DepartmentsRepository;
use App\Repositories\Post\LocationRepository;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    protected $locationRepository;
    protected $department;
    public function __construct(LocationRepository $locationRepository, DepartmentsRepository $department)
    {
        $this->locationRepository = $locationRepository;
        $this->department = $department;
    }

    public function index()
    {
        $departments = $this->department->all();
        $locations = $this->locationRepository->with(['department'])->all();
        return view('Location',compact('locations','departments'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateLocationRequest $request)
    {
        $data = $request->param();
//        dd($data);
        // tạo validate

        $location = $this->locationRepository->create($data);


        return response()->json(['data'=> $data,'location ' =>$location]);


    }
    public function importData(ImportDataRequest $request){

        if ($request->hasFile('csv_file')) {
            try {

                $data = file($request['csv_file']);
                ImportQueuesJob::dispatch($data);
                return redirect()->route('upload_file');
            } catch (Exception $e) {
                return 'Lỗi: ' . $e->getMessage();
            }
        }

    }

    public function upload_file()
    {
        $path = storage_path('temp');
        $files = glob($path . '/*.csv');
        foreach ($files as $key => $file) {
            $csv = array_map('str_getcsv', file($file));
            if ($key == 0) {
                $header = $csv[0];
                $header = array_map(function ($values) {
                    return str_replace(' ', '_', strtolower($values));
                }, $header);
                unset($csv[0]);
            }
            foreach ($csv as $values) {
                if (count($header) !== count($values)) {
                    return count($header) . 'Lỗi: Số lượng cột không khớp' . count($values);
                }
                $record = array_combine($header, $values);
                foreach ($record as $key => $value) {
                    $cleanedKey = str_replace("\u{FEFF}", '', $key);
                    $record[$cleanedKey] = $value;

                    if ($cleanedKey !== $key) {
                        unset($record[$key]);
                    }
                }

//                dd($record);
                if (is_numeric($record['department_id'])) {

                    $location = $this->locationRepository->create($record);
                    if ($location)
                    {
                        if (File::exists(($file))) {
                            File::delete(($file));
                        }
                        Session::flash('message', trans('messages.success'));
                        return redirect()->route('locations');
                    }
                    else{
                        Session::flash('message', trans('messages.failure')); // For failure
                        return redirect()->route('locations');
                    }
                } else {
                    Session::flash('message', trans('messages.failure'));
                   return redirect()->route('locations');
                }


            }

        }

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $location =  $this->locationRepository->with(['department'])->getById($id);
        return response()->json($location);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $data = $this->locationRepository->deleteById($id);
        return response()->json(['data'=>$data]);
    }

}
