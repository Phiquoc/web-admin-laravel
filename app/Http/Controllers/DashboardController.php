<?php

namespace App\Http\Controllers;

use App\Http\Resources\ListUserResource;
use App\Repositories\Post\AssetRepository;
use App\Repositories\Post\UserRepository;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    protected $userRepository;
    protected $assetRepository;
    public function __construct(UserRepository $userRepository, AssetRepository $assetRepository){
        $this->userRepository = $userRepository;
        $this->assetRepository=$assetRepository;
    }
    public function getRecentAssets(){
        $recentAssets = $this->assetRepository->getRecentAssets(10);
        return response()->json($recentAssets);
    }
    public function getTotalAssets(){
        $totalAsset = $this->assetRepository->all();
        return response()->json($totalAsset);
    }
    public function getRecentUsers(){
        $recentUser = $this->userRepository->getRecentUser(10);
        $user = ListUserResource::collection($recentUser);
        return response()->json($user);
    }
    public function getTotalUsers(){
        $allUser = $this->userRepository->getAll();
        $user = ListUserResource::collection($allUser);
        return response()->json($user);
    }
}
