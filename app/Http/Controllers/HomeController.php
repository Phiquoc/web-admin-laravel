<?php

namespace App\Http\Controllers;

use App\Models\Assets;
use App\Models\User;
use App\Repositories\Post\AssetRepository;
use App\Repositories\Post\UserRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $UserRepository;
    protected $AssetRepository;

    public function __construct(UserRepository $UserRepository, AssetRepository $AssetRepository)
    {
        $this->AssetRepository = $AssetRepository;
        $this->UserRepository = $UserRepository;
    }

    /**
     * Show the application dashboard.
     *

     */

    public function index(Request $request)

    {

        $assets = Assets::with([ 'locations', 'country']) ->get();
        $users = User::with(['country', 'role', 'department'])->get();
        return view('index')->with(compact('users', 'assets'));
    }

    public function getSomeResource(Request $request)
    {
        $jwtCookie = $request->cookie('jwt'); // Lấy token từ cookie

        if ($jwtCookie) {
            $request->headers->set('Authorization', 'Bearer ' . $jwtCookie);
            $user = $request->user();
            return $user;
        }
        return false;
    }

}
