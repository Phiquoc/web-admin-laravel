<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Location\CopyLocationRequest;
use App\Http\Requests\Location\CreateLocationRequest;
use App\Http\Requests\Location\ExcelLocationRequest;
use App\Http\Requests\Location\ResearchLocationRequest;
use App\Http\Requests\Location\UpdateLocationRequest;
use App\Jobs\ImportQueuesJob;
use App\Repositories\Post\LocationRepository;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;

class LocationsController extends Controller
{
    protected  $locationRepository;
    /**
     * Display a listing of the resource.
     */
    public function __construct(LocationRepository $locationRepository){
        $this->locationRepository=$locationRepository;
    }
    public function index()
    {

        $locations = $this->locationRepository->with(['department'])->paginate(10);
        return response()->json($locations);
    }

    public function search(ResearchLocationRequest $request)
    {
        $searchTerm = $request->input('name');
        $locations = $this->locationRepository->where('name',  '%'.$searchTerm.'%','like')->get();
        return response()->json(['data'=>$locations, 'key_sreach' => $searchTerm]);
    }
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateLocationRequest $request)
    {
        $data = $request->param();
        $location = $this->locationRepository->create($data);
        return response()->json(['data'=>$data,'location'=>$location]);
    }

    public function copy($id){

        $location = $this->locationRepository->getById($id);
        if ($location)
        {
            $location['name'] = $location['name'].'_copy';
            $attributes = [
                'name' => $location->name,
                'note' => $location->note,
                'department_id' => $location->department_id,
            ];
            $copy_location = $this->locationRepository->create($attributes);
            return response()->json(['data'=>$copy_location],200);
        }
        else
        {
            return response()->json(['error'=>'Not find location'],201);
        }

    }

    public function import(ExcelLocationRequest $request){

        $validatedData = $request->validated();

        if ($request->hasFile('file')) {
            try {

                $data = file($validatedData['file']);
                ImportQueuesJob::dispatch($data);
                return $this->upload_file();
            } catch (\Exception $e) {
                $errorResponse = (['error' => 'Lỗi: ' . $e->getMessage()]);
                return response()->json($errorResponse, 500);
            }
        }

    }

    public function upload_file()
    {
        $path = storage_path('temp');
        $files = glob($path . '/*.csv');

        foreach ($files as $file) {
            $csv = array_map('str_getcsv', file($file));
            $response = [];

            if (count($csv) === 0) {
                // Handle an empty CSV file
                $response['message'] = 'Empty CSV file';
                return response()->json($response, 400);
            }

            if (count($csv[0]) === 0) {
                // Handle CSV files with no header
                $response['message'] = 'CSV file has no header';
                return response()->json($response, 400);
            }

            $header = $csv[0];
            $header = array_map(function ($value) {
                return str_replace(' ', '_', strtolower($value));
            }, $header);
            unset($csv[0]);


            foreach ($csv as $values) {
                if (count($header) !== count($values)) {
                    $response['message'] = 'Column count does not match'.count($header) .count($values) ;
                    return response()->json($response, 400);
                }

                $record = array_combine($header, $values);
                foreach ($record as $key => $value) {
                    $cleanedKey = str_replace("\u{FEFF}", '', $key);
                    $record[$cleanedKey] = $value;

                    if ($cleanedKey !== $key) {
                        unset($record[$key]);
                    }
                }
                if (isset($record['department_id']) && is_numeric($record['department_id'])) {
                    $location = $this->locationRepository->create($record);

                    if ($location) {
                        if (File::exists($file)) {
                            File::delete($file);
                        }

                        $response['message'] = 'Success';
                    } else {
                        $response['message'] = 'Insertion failed';
                    }
                } else {
                    $response['message'] = $record;
                }
            }
        }

        return response()->json($response);

    }

    /**
     * Display the specified resource.
     */

    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateLocationRequest $request, string $id)
    {
        $data = $request->param();
        if($data['name'] === null){
            unset($data['name']);
        }
        if($data['department_id'] === null){
            unset($data['department_id']);
        }
        if($data['note'] === null){
            unset($data['note']);
        }
        $location = $this->locationRepository->updateById($id,$data);
        return response()->json(['data' => $location]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $location = $this->locationRepository->deleteById($id);
        return response()->json(['data'=>$location]);
    }
}
