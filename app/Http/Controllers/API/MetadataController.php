<?php


namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\Post\CategoryRepository;
use App\Repositories\Post\CountryRepository;
use App\Repositories\Post\DepartmentsRepository;
use App\Repositories\Post\ManufacturersRepository;
use App\Repositories\Post\ModelsRepository;
use App\Repositories\Post\SupplierRepository;


class MetadataController extends Controller
{
    protected $supplierRepository;
    protected $modelsRepository;
    protected $countryRepository;
    protected $departmentRepository;
    protected $manufacturersRepository;
    protected $categoryRepository;

    public function __construct(
        SupplierRepository $supplierRepository,
        CountryRepository $countryRepository,
        DepartmentsRepository $departmentRepository,
        ModelsRepository $modelsRepository,
        ManufacturersRepository $manufacturersRepository,
        CategoryRepository $categoryRepository
    ) {
        $this->supplierRepository = $supplierRepository;
        $this->countryRepository = $countryRepository;
        $this->departmentRepository = $departmentRepository;
        $this->modelsRepository = $modelsRepository;
        $this->manufacturersRepository = $manufacturersRepository;
        $this->categoryRepository = $categoryRepository;
    }
    public function getDepartments()
    {
        $departments = $this->departmentRepository->all();
        return response()->json($departments);
    }

    public function getCountries()
    {
        $countries = $this->countryRepository->all();
        return response()->json($countries);
    }
    public function getManufacturers()
    {
        $manufacturers = $this->manufacturersRepository->get();

        if ($manufacturers) {
            return response()->json($manufacturers);
        } else {
            return response()->json(['message' => 'Không tìm thấy nhà sản xuất.'], 404);
        }
    }
    public function getCategories()
    {
        $Category =  $this->categoryRepository->all();
        return response()->json($Category);
    }
    public function getModels()
    {
        $models = $this->modelsRepository->all();
        return response()->json($models);
    }
    public function getSuppliers()
    {
        $suppliers =  $this->supplierRepository->all();
        return response()->json($suppliers);
    }
}
