<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ListUserResource;
use App\Jobs\EmailQueuesJob;
use App\Models\User;
use App\Repositories\Post\UserRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;

class AuthController extends Controller
{
    protected $UserRepository;


    public function __construct(UserRepository $UserRepository)
    {

        $this->UserRepository = $UserRepository;
    }

    public function login(Request $request)
    {

        $token = null;
        $input = $request->only('email', 'password');
        if (!$token = JWTAuth::attempt($input)) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], Response::HTTP_UNAUTHORIZED);
        } else {
            $cookie = Cookie::make(config('jwt.cookie'), $token, 60 * 24);
            return response()->json([
                'success' => true,
                'token' => $token,
            ])->withCookie($cookie);
        }

    }

//    public function login(Request $request)
//    {
//        $credentials = $request->only('email', 'password');
//
//        if (!$token = JWTAuth::attempt($credentials)) {
//            return response()->json(['error' => 'Unauthorized'], 401);
//        }
//
//        return $this->respondWithToken($token);
//    }
    private function respondWithToken($token)
    {
        $user = Auth::user();
        $userList = new ListUserResource($user);
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'user' => $userList,
        ]);
    }

    public function logout()
    {
        Auth::logout();
        $cookie=Cookie::forget(config('jwt.cookie'));
        return response()->json(['message' => 'Đăng xuất thành công'], 200)->withCookie($cookie);
    }
    public function register(Request $request)
    {
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        $data['status'] = 'inactive';
        $data['role_id'] = 1;
        $token = $data['remember_token'] = Str::random(6);
        $data['verification_code_expires_at'] = now()->addMinutes(5);
        $name = $data['name'];
        $email = $data['email'];

        // Create a new user
        $user = $this->UserRepository->create($data);

        if ($user) {
            // Send a verification email asynchronously (you may need to adjust this part)
            $sendJob = (new EmailQueuesJob($name, $email, $token))->delay(Carbon::now()->addSeconds(5));
            dispatch($sendJob);
            $token = $user->createToken('MyApp');
            $plaintextToken = $token->plainTextToken;
            $user->update(['api_token' => $plaintextToken]);
            $userResource = new ListUserResource($user);
            return response()->json(['message' => 'Registration successful', 'user' => $userResource], 200);
        } else {
            return response()->json(['error' => 'Registration failed'], 500);
        }
    }

    public function verifyEmail(Request $request)
    {
        $email = $request->input('email');
        $token = $request->input('token');
        $user = User::where('email', $email)->first();
        $now = Carbon::now();
        $expiresAt = Carbon::parse($user->verification_code_expires_at);
        $minutesDifference = $now->diffInMinutes($expiresAt);
        if ($minutesDifference > 5) {
            return response()->json(['error' => 'Over token timeout'], 400);
        } else if ($user) {
            if ($user->status == 'inactive') {
                if ($user->remember_token === $token) {
                    $update = $user->update(['status' => 'active']);
                    if ($update) {
                        return response()->json(['message' => 'Email verification successful'], 200);
                    } else {
                        return response()->json(['error' => 'Email have been active'], 400);
                    }
                } else {
                    return response()->json(['error' => 'Code wrong'], 400);
                }
            } else {
                return response()->json(['error' => 'Email has been active'], 400);
            }
        } else {
            return response()->json(['error' => 'Email verification failed'], 400);
        }
    }
    public function user_profile(Request $request)
    {

        $token =  $request->cookie(config("jwt.cookie"));
        if ($token) {
            JWTAuth::setToken($token);
            try {
                $user = JWTAuth::user();

                return response()->json(['user' => $user]);
            } catch (JWTException $e) {

                return response()->json(['error' => 'Error: ' . $e->getMessage()], 500); // 500 là mã lỗi HTTP cho lỗi máy chủ
            }
        }


    }
}
