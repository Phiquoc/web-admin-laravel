<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateAssetRequest;
use App\Http\Requests\Location\ExcelLocationRequest;
use App\Http\Requests\UpdateAssetRequest;
use App\Imports\ImportAsset;
use App\Jobs\ImportQueuesJob;
use App\Models\Assets;
use App\Repositories\Post\AssetRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    protected $assetRepository;
    public function __construct(AssetRepository $assetRepository)
    {
        $this->assetRepository=$assetRepository;
    }

    public function index()
    {
        $data =$this->assetRepository->with('locations','condition','country')->paginate(10);
        return response()->json(['data' => $data]);
    }

    public function search( Request $request)
    {
        $search = $request->input('search', ''); // Search by asset name or asset code

       $assets= $this->assetRepository->searchByNameAndCodeName($search);



        return response()->json($assets);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateAssetRequest $request)
    {
        $data = $request->param();

            do {
                $randomCode = Str::random(7);
            } while ($this->assetRepository->exists('asset_code',$randomCode));
            $data['asset_code'] = $randomCode;
            $create = $this->assetRepository->create($data);
            if ($create)
            {
                return response()->json(['data'=>$create],200);
            }
            else {
                return response()->json(['error' => 'Error when create data'], 201);
            }


    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateAssetRequest $request, string $id)
    {
        $data = $request->all();
        $update_asset = $this->assetRepository->updateById($id,$data);
        if ($update_asset)
        {
            return response()->json(['data'=>$update_asset],200);
        }
        else{
            return  response()->json(['error'=>'Error when create data'],201);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function QRcode($id){
        $code = $this->assetRepository->find($id)->asset_code;
        $qrCode = QrCode::format('png')->size(200)->generate($code);
        return response($qrCode)->header('Content-Type', 'image/png');
    }
    public function destroy(string $id)
    {
        $delete_asset= $this->assetRepository->deleteById($id);
        return response()->json(['data'=>$delete_asset],200);
    }
    public function import(ExcelLocationRequest $request)
    {
        $validatedData = $request->validated();

        if ($request->hasFile('file')) {
            try {
                $data = file($validatedData['file']);
                ImportQueuesJob::dispatch($data);
                return $this->upload_file();
            } catch (\Exception $e) {
                return response()->json(['Lỗi: ' . $e->getMessage()]);
            }
        }
    }

    public function upload_file(){
        return 1;
    }

}
