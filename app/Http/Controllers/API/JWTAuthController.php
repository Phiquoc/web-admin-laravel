<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\ListUserResource;
use App\Jobs\EmailQueuesJob;
use App\Models\User;
use App\Repositories\Post\UserRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cookie;
use Tymon\JWTAuth\Facades\JWTFactory;

class JWTAuthController extends Controller
{
    public $token = true;
    protected $UserRepository;

    public function __construct(UserRepository $UserRepository)
    {
        $this->middleware('auth:api',[
            'except'=>[
                'login',
                'register'
            ]
        ]);
        $this->UserRepository = $UserRepository;
    }

    public function register(RegisterRequest $request)
    {
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        $data['status'] = 'inactive';
        $data['role_id'] = 1;
        $token = $data['remember_token'] = Str::random(6);
        $data['verification_code_expires_at'] = now()->addMinutes(5);
        $name = $data['name'];
        $email = $data['email'];

        $user = $this->UserRepository->create($data);

        if ($user) {
            $sendJob = (new EmailQueuesJob($name, $email, $token))->delay(Carbon::now()->addSeconds(5));
            dispatch($sendJob);
//            $token = $user->createToken('MyApp');
//            $plaintextToken = $token->plainTextToken;
            $token = JWTAuth::login($user);
            $user->update(['api_token' => $token]);
            $userResource = new ListUserResource($user);
            return response()->json([
                'message' => 'Registration successful',
                'user' => $userResource,
                'token'=>$token
            ], 200);
        } else {
            return response()->json(['error' => 'Registration failed'], 500);
        }
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }
    private function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => JWTFactory::getTTL() * 60,
        ]);
    }
//    public function login(Request $request)
//    {
//        $token = null;
//        $input = $request->only('email', 'password');
//        if (!$token = JWTAuth::attempt($input)) {
//            return response()->json([
//                'success' => false,
//                'message' => 'Invalid Email or Password',
//            ], Response::HTTP_UNAUTHORIZED);
//        } else {
//            $cookie = Cookie::make(config('jwt.cookie'), $token, 60 * 24);
//            return response()->json([
//                'success' => true,
//                'token' => $token,
//            ])->withCookie($cookie);
//        }
//    }
    protected function createNewToken($token)
    {
        // Chú ý rằng 'access_token' ở đây sẽ chứa giá trị của mã thông báo
        $user = auth()->user();
        $userList = new ListUserResource($user);
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => JWTFactory::getTTL() * 60,
            'user' => $userList,
        ]);
    }
    public function logout(Request $request)
    {

        try {
            $token = $request->cookie(config("jwt.cookie"));

            JWTAuth::invalidate($token);

            $cookie = Cookie::forget(config('jwt.cookie'));

            return response()->json([
                'success' => true,
                'message' => 'User logged out successfully'
            ])->withCookie($cookie);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, the user cannot be logged out'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function user_profile(Request $request)
    {

        $token =  $request->cookie(config("jwt.cookie"));
        if ($token) {
            JWTAuth::setToken($token);
            try {
                $user = JWTAuth::user();

                return response()->json(['user' => $user]);
            } catch (JWTException $e) {

                return response()->json(['error' => 'Error: ' . $e->getMessage()], 500); // 500 là mã lỗi HTTP cho lỗi máy chủ
            }
        }


    }
}
