<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ForgotRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;


class ResetPasswordController extends Controller
{
    public function showResetPasswordForm($token){
        return view('auth.reset-password', ['token' => $token]);
    }
    public function reset(ForgotRequest $request)
    {
        $request = Request::capture(); // Đảm bảo bạn đã import Request ở đầu tệp
        $referer = $request->server('HTTP_REFERER');
        $email = Str::after($referer, '?email=');
        $decodedEmail = urldecode($email);
        $status = Password::reset(array_merge(
            $request->only('password', 'password_confirmation', 'token'),
            ['email' => $decodedEmail]
        ), function ($user, $password) {
            $user->forceFill([
                'password' => \Illuminate\Support\Facades\Hash::make($password),
            ])->save();
        });

        if ($status == Password::PASSWORD_RESET) {
            return redirect()->route('login')->with('status', 'Mật khẩu đã được đặt lại thành công.');
        } else {
            return back()->withErrors(['email' => [__($status)]]);
        }
    }
}
