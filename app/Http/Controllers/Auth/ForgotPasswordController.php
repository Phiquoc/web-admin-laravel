<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmailRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    public function index(){
        return view('auth.forgot-password');
    }
    public function password_send(EmailRequest $request){


        $status = Password::sendResetLink(
            $request->only('email')
        );
        if($status === Password::RESET_LINK_SENT)
        {
            return back()->with(['status' => __($status)]);
        }
        else{
            return back()->withErrors(['email' => __($status)]);
        }
    }
}
