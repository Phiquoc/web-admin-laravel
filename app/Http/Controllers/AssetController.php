<?php

namespace App\Http\Controllers;

use App\Models\Assets;
use Illuminate\Http\Request;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Assets $asset)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Assets $asset)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Assets $asset)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Assets $asset)
    {
        //
    }
}
