<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Repositories\Post\UserRepository;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    protected $UserRepository;

    public function __construct(UserRepository $UserRepository)
    {
        $this->UserRepository = $UserRepository;
    }
    public function index()
    {
        if (Auth::check()) {
            return redirect()->route('home');
        }
        return view('auth.login');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */


    public function login(Request $request)
    {
        $token = null;
        $credentials = $request->only('email', 'password');
        if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], Response::HTTP_UNAUTHORIZED);
        }
        else
        {
            $user = Auth::user();
            if ($user->status == 'active') {
                $cookie = Cookie::make(config('jwt.cookie'), $token, 60 * 24);

                return redirect()->route('home')->withCookie($cookie);
            } else {
                return redirect()->route('verify', ['email' => $user->email])->with('error', 'The account has not been activated yet');
            }
        }
    }


    public function logout(Request $request)
    {
        Auth::logout();

        $cookie = Cookie::forget(config('jwt.cookie'));
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/log-in')->withCookie($cookie);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */

}
