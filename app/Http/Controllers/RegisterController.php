<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Jobs\EmailQueuesJob;
use App\Mail\VerifyEmail;
use App\Repositories\Post\UserRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    protected $UserRepository;

    public function __construct(UserRepository $UserRepository)
    {
        $this->UserRepository = $UserRepository;
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(RegisterRequest $request)
    {
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        $data['status'] = 'inactive';
        $data['role_id'] = 1;
        $token = $data['remember_token'] = Str::random(6);
        $data['verification_code_expires_at'] = now()->addMinutes(5);
        $name = $data['name'];
        $email = $data['email'];

        $users = $this->UserRepository->create($data);
        if ($users) {
            $sendJob = (new EmailQueuesJob($name, $email, $token))->delay(Carbon::now()->addSeconds(5));
            dispatch($sendJob);
            $user = $this->UserRepository->find($users->id);
            $token = $user->createToken('MyApp');
            $plaintextToken = $token->plainTextToken;
            
            $user_token = $this->UserRepository->update($users->id, ['api_token' => $plaintextToken]);

            if ($user_token) {
                return redirect()->route('verify', ['email' => $email]);
            }
        } else {
            return false;
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
