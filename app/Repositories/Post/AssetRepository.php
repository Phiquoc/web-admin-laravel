<?php

namespace App\Repositories\Post;

use App\Models\Assets;
use Illuminate\Support\Facades\DB;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class AssetRepository.
 */
class AssetRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Assets::class;
    }
    public function create(array $data)
    {
        DB::beginTransaction(); // Begin the transaction
        try {
            $this->unsetClauses();
            $record=$this->model->create($data);
            DB::commit();
            return response()->json(['message' => 'Transaction committed successfully', 'data' => $record], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Transaction failed', 'error' => $e->getMessage()], 500);
        }
    }
    public function getRecentAssets($limit)
    {
        return $this->model->orderBy('created_at', 'desc')
            ->take($limit)
            ->get();
    }
    public function searchByNameAndCodeName($search)
    {
        return $this->model
            ->where(function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%')
                    ->orWhere('asset_code', 'like', '%' . $search . '%');
            })->with('locations','condition','country')
            ->get();
    }
    public function find($id)
    {
        $result = $this->model->find($id);

        return $result;
    }
    public function exists($field, $value)
    {
        return $this->model->where($field, $value)->exists();
    }

}
