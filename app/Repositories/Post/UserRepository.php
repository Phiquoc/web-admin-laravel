<?php

namespace App\Repositories\Post;

use App\Models\User;
use App\Repositories\EloquentRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

//use Your Model

/**
 * Class RegisterRepository.
 */
class UserRepository extends EloquentRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function getModel()
    {
        return User::class;
    }
    public function register($data)
    {
        try {
            DB::beginTransaction();
            $model = User::create($data);
            DB::commit();
            return $model;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Create failed: ' . $e->getMessage());
            return null;
        }
    }
    public function getRecentUser($limit)
    {
        return User::orderBy('created_at', 'desc')
            ->take($limit)
            ->get();
    }

    public function findemail($email)
    {
        $email = $this->_model->where('email', $email)->first();
        return $email;
    }
}
