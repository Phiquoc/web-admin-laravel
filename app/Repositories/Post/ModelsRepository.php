<?php

namespace App\Repositories\Post;

use App\Models\Models;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class ModelsRepository.
 */
class ModelsRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Models::class;   
    }
}
