<?php

namespace App\Repositories\Post;

use App\Models\Department;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class DepartmentsRepository.
 */
class DepartmentsRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Department::class;
    }
}
