<?php

namespace App\Repositories\Post;

use App\Models\Manufactory;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class ManufacturersRepository.
 */
class ManufacturersRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Manufactory::class;
    }
}
