<?php

namespace App\Repositories\Post;

use App\Models\Suppliers;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class SupplierRepository.
 */
class SupplierRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Suppliers::class;
    }
}
