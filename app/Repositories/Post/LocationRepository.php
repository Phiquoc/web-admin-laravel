<?php

namespace App\Repositories\Post;

use App\Models\Locations;
use Illuminate\Support\Facades\DB;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;

//use Your Model

/**
 * Class LocationRepository.
 */
class LocationRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Locations::class;
    }


    public function create(array $data)
    {
        DB::beginTransaction(); 
        try {
            $now = now();
            $data['created_at'] = $now;
            $data['updated_at'] = $now;
            $this->model->insert($data);
            DB::commit();
            return response()->json(['message' => 'Transaction committed successfully', 'data' => $data], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Transaction failed', 'error' => $e->getMessage()], 500);
        }

    }


    /**
     * Delete
     *
     * @param $id
     * @return bool
     */


}
