<?php

namespace App\Imports;

use App\Models\Locations;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithValidation;

class LocationDataImport implements ToModel,WithValidation,ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        //
    }
    public function model(array $row)
    {
        if (!is_numeric($row[2])) {

            $row[2] = 0;
        }
        return new Locations([
            'name' => $row[0],
            'note' => $row[1],
            'department_id' => (int)$row[2],
        ]);
    }

    public function rules(): array
    {
        return [
            '0' => ['required', 'string'],
            '1' => ['required', 'string'],
            '2' => ['required', 'integer'],
        ];
    }
}
