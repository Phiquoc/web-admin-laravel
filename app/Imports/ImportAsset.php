<?php

namespace App\Imports;

use App\Models\Assets;
use Maatwebsite\Excel\Concerns\ToModel;

class ImportAsset implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Assets([
            'name' => $row[0],
            'condition' => $row[1],
            'note' => ($row[2]),
            'asset_code' => $row[3],
            'category_id' => $row[4],
            'country_id' => ($row[5]),
            'location_id' =>$row[6],
        ]);
    }
}
