<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = "department";
    use HasFactory;
    public function users()
    {
        return $this->hasMany(User::class, 'department_id');
    }
    public function assets()
    {
        return $this->hasMany(Locations::class, 'department_id');
    }
}
