<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class app_countries extends Model
{
    protected $table = "apps_countries";
    public function users()
    {
        return $this->hasMany(User::class, 'country_id');
    }
    public function assets()
    {
        return $this->hasMany(Assets::class, 'country_id');
    }
    use HasFactory;
}
