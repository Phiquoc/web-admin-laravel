<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Assets extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'condition', 'note', 'asset_code', 'category_id', 'country_id', 'location_id'];

    public function categories()
    {
        return $this->belongsTo(Categories::class, 'category_id');
    }

    public function locations()
    {
        return $this->belongsTo(Locations::class, 'location_id');
    }

    public function country()
    {
        return $this->belongsTo(app_countries::class, 'country_id');
    }
    public function condition(){
        return $this->belongsTo(Condition::class,'condition_id');
    }
}
