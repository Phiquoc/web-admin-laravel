<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Locations extends Model
{

    use HasFactory;
    public function assets()
    {
        return $this->hasMany(Assets::class, 'location_id');
    }
    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }
    protected $fillable = ['name','note','department_id'];

}
