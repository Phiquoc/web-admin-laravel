/**
* Template Name: NiceAdmin
* Updated: Sep 18 2023 with Bootstrap v5.3.2
* Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
* Author: BootstrapMade.com
* License: https://bootstrapmade.com/license/
*/
(function() {
  "use strict";

  /**
   * Easy selector helper function
   */
  const select = (el, all = false) => {
    el = el.trim()
    if (all) {
      return [...document.querySelectorAll(el)]
    } else {
      return document.querySelector(el)
    }
  }

  /**
   * Easy event listener function
   */
  const on = (type, el, listener, all = false) => {
    if (all) {
      select(el, all).forEach(e => e.addEventListener(type, listener))
    } else {
      select(el, all).addEventListener(type, listener)
    }
  }

  /**
   * Easy on scroll event listener
   */
  const onscroll = (el, listener) => {
    el.addEventListener('scroll', listener)
  }

  /**
   * Sidebar toggle
   */
  if (select('.toggle-sidebar-btn')) {
    on('click', '.toggle-sidebar-btn', function(e) {
      select('body').classList.toggle('toggle-sidebar')
    })
  }

  /**
   * Search bar toggle
   */
  if (select('.search-bar-toggle')) {
    on('click', '.search-bar-toggle', function(e) {
      select('.search-bar').classList.toggle('search-bar-show')
    })
  }

  /**
   * Navbar links active state on scroll
   */
  let navbarlinks = select('#navbar .scrollto', true)
  const navbarlinksActive = () => {
    let position = window.scrollY + 200
    navbarlinks.forEach(navbarlink => {
      if (!navbarlink.hash) return
      let section = select(navbarlink.hash)
      if (!section) return
      if (position >= section.offsetTop && position <= (section.offsetTop + section.offsetHeight)) {
        navbarlink.classList.add('active')
      } else {
        navbarlink.classList.remove('active')
      }
    })
  }
  window.addEventListener('load', navbarlinksActive)
  onscroll(document, navbarlinksActive)

  /**
   * Toggle .header-scrolled class to #header when page is scrolled
   */
  let selectHeader = select('#header')
  if (selectHeader) {
    const headerScrolled = () => {
      if (window.scrollY > 100) {
        selectHeader.classList.add('header-scrolled')
      } else {
        selectHeader.classList.remove('header-scrolled')
      }
    }
    window.addEventListener('load', headerScrolled)
    onscroll(document, headerScrolled)
  }

  /**
   * Back to top button
   */
  let backtotop = select('.back-to-top')
  if (backtotop) {
    const toggleBacktotop = () => {
      if (window.scrollY > 100) {
        backtotop.classList.add('active')
      } else {
        backtotop.classList.remove('active')
      }
    }
    window.addEventListener('load', toggleBacktotop)
    onscroll(document, toggleBacktotop)
  }

  /**
   * Initiate tooltips
   */
  var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
  var tooltipList = tooltipTriggerList.map(function(tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl)
  })

  /**
   * Initiate quill editors
   */
  if (select('.quill-editor-default')) {
    new Quill('.quill-editor-default', {
      theme: 'snow'
    });
  }

  if (select('.quill-editor-bubble')) {
    new Quill('.quill-editor-bubble', {
      theme: 'bubble'
    });
  }

  if (select('.quill-editor-full')) {
    new Quill(".quill-editor-full", {
      modules: {
        toolbar: [
          [{
            font: []
          }, {
            size: []
          }],
          ["bold", "italic", "underline", "strike"],
          [{
              color: []
            },
            {
              background: []
            }
          ],
          [{
              script: "super"
            },
            {
              script: "sub"
            }
          ],
          [{
              list: "ordered"
            },
            {
              list: "bullet"
            },
            {
              indent: "-1"
            },
            {
              indent: "+1"
            }
          ],
          ["direction", {
            align: []
          }],
          ["link", "image", "video"],
          ["clean"]
        ]
      },
      theme: "snow"
    });
  }

  /**
   * Initiate TinyMCE Editor
   */
  const useDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;
  const isSmallScreen = window.matchMedia('(max-width: 1023.5px)').matches;

  tinymce.init({
    selector: 'textarea.tinymce-editor',
    plugins: 'preview importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap pagebreak nonbreaking anchor insertdatetime advlist lists wordcount help charmap quickbars emoticons',
    editimage_cors_hosts: ['picsum.photos'],
    menubar: 'file edit view insert format tools table help',
    toolbar: 'undo redo | bold italic underline strikethrough | fontfamily fontsize blocks | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
    toolbar_sticky: true,
    toolbar_sticky_offset: isSmallScreen ? 102 : 108,
    autosave_ask_before_unload: true,
    autosave_interval: '30s',
    autosave_prefix: '{path}{query}-{id}-',
    autosave_restore_when_empty: false,
    autosave_retention: '2m',
    image_advtab: true,
    link_list: [{
        title: 'My page 1',
        value: 'https://www.tiny.cloud'
      },
      {
        title: 'My page 2',
        value: 'http://www.moxiecode.com'
      }
    ],
    image_list: [{
        title: 'My page 1',
        value: 'https://www.tiny.cloud'
      },
      {
        title: 'My page 2',
        value: 'http://www.moxiecode.com'
      }
    ],
    image_class_list: [{
        title: 'None',
        value: ''
      },
      {
        title: 'Some class',
        value: 'class-name'
      }
    ],
    importcss_append: true,
    file_picker_callback: (callback, value, meta) => {
      /* Provide file and text for the link dialog */
      if (meta.filetype === 'file') {
        callback('https://www.google.com/logos/google.jpg', {
          text: 'My text'
        });
      }

      /* Provide image and alt text for the image dialog */
      if (meta.filetype === 'image') {
        callback('https://www.google.com/logos/google.jpg', {
          alt: 'My alt text'
        });
      }

      /* Provide alternative source and posted for the media dialog */
      if (meta.filetype === 'media') {
        callback('movie.mp4', {
          source2: 'alt.ogg',
          poster: 'https://www.google.com/logos/google.jpg'
        });
      }
    },
    templates: [{
        title: 'New Table',
        description: 'creates a new table',
        content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>'
      },
      {
        title: 'Starting my story',
        description: 'A cure for writers block',
        content: 'Once upon a time...'
      },
      {
        title: 'New list with dates',
        description: 'New List with dates',
        content: '<div class="mceTmpl"><span class="cdate">cdate</span><br><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>'
      }
    ],
    template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
    template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
    height: 600,
    image_caption: true,
    quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
    noneditable_class: 'mceNonEditable',
    toolbar_mode: 'sliding',
    contextmenu: 'link image table',
    skin: useDarkMode ? 'oxide-dark' : 'oxide',
    content_css: useDarkMode ? 'dark' : 'default',
    content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:16px }'
  });

  /**
   * Initiate Bootstrap validation check
   */
  var needsValidation = document.querySelectorAll('.needs-validation')

  Array.prototype.slice.call(needsValidation)
    .forEach(function(form) {
      form.addEventListener('submit', function(event) {
        if (!form.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }

        form.classList.add('was-validated')
      }, false)
    })

  /**
   * Initiate Datatables
   */
  const datatables = select('.datatable', true)
  datatables.forEach(datatable => {
    new simpleDatatables.DataTable(datatable);
  })

  /**
   * Autoresize echart charts
   */
  const mainContainer = select('#main');
  if (mainContainer) {
    setTimeout(() => {
      new ResizeObserver(function() {
        select('.echart', true).forEach(getEchart => {
          echarts.getInstanceByDom(getEchart).resize();
        })
      }).observe(mainContainer);
    }, 200);
  }
    $(document).on('change', '#asset_manufactory_create', function() {
        let id = $(this).val();

        let result =''
        if (id !== '')
        {
            $('.model').css('display','block')
            $.ajax({
                headers:{
                    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                },
                url: `/api/model/choose-model/${id}`,
                type:'POST',
                success:function(data){
                    $('#asset_model_create').html(data);
                }
            })

        }
        else {
            $('.model').css('display','none')
        }

    });
    $(document).ready(function() {

        $('.checkbox').on('change', function() {
            if ($(this).is(':checked')) {
                // Nếu checkbox được chọn, thực hiện hành động ở đây
                var checkboxIndex = $(this).attr('id').replace('checkbox-', ''); // Lấy index của checkbox
                // Thực hiện hành động dựa trên checkboxIndex
                alert('Checkbox ' + checkboxIndex + ' đã được chọn.');
            } else {
                // Nếu checkbox không được chọn, thực hiện hành động khác (nếu cần)
            }
        });
    });

    $('.asset_search').on('input', function() {
        var searchTerm = $(this).val();
        $.ajax({
            url: '/api/asset/search',
            type: 'POST',
            data: {
                search: searchTerm
            },
            success: function (response) {

                var table = $('.asset_table tbody');
                table.empty();
                if(response.length <= 0)
                {
                    table.html('<tr><td colspan="7" style="text-align:center;">Not found</td></tr>');
                }
                else {

                    $.each(response, function (index, item) {
                        var row = '<tr data-index="' + index + '">' +
                            '<th scope="row">' + (index + 1) + '</th>' +
                            '<td>' + item.asset_code + '</td>' +
                            '<td>' + item.name + '</td>' +
                            '<td>' + item.locations.name + '</td>' +
                            '<td>' +
                            // '<button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="openCopyModal(' + item.id + ')"> <i class="bx bxs-copy-alt"></i> </button>' +
                            // '<button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="openEditModal(' + item.id + ')"> <i class="bx bxs-edit"></i> </button>' +
                            // '<button class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="openDeleteModal(' + item.id + ')"> <i class="ri-chat-delete-fill"></i> </button>' +
                            '</td>' +
                            '<td></td>' +
                            '<td>' + item.note + '</td>' +

                            '</tr>';
                        table.append(row);
                    });
                }


            },
            error: function (error) {

            }
        });
    });
    $('.asset_qr_search').on('input', function() {
        var searchTerm = $(this).val();
        $.ajax({
            url: '/api/asset/search',
            type: 'POST',
            data: {
                search: searchTerm
            },
            success: function (data) {

                var table = $('#qr');
                table.empty();
                if(data.length <= 0)
                {
                    table.html('<tr><td colspan="7" style="text-align:center;">Not found</td></tr>');
                }
                else {

                    $.each(data, function(index, item) {
                        var id = item.id;
                        var infoLeftHTML = '<div class="row card" style="border: 2px solid #1a1d20; height: 190px; margin-bottom: 20px">';
                        infoLeftHTML += '<div class="col-md-6">';
                        infoLeftHTML += '<strong>' + item.name + '</strong>';
                        infoLeftHTML += '<p>Asset Code: ' + item.asset_code + '</p>';
                        infoLeftHTML += '</div>';
                        infoLeftHTML += '<div class="d-flex justify-content-end col-md-6">';
                        infoLeftHTML += '<div class="row">';
                        infoLeftHTML += '<img id="qr-code-' + index + '" src="http://quocphi-intern.loc/api/asset/qr-code/' + id + '" ' +
                            'style="margin-top:20px; width: 300px; height: 100px;">';
                        infoLeftHTML += '<div class="row">';
                        infoLeftHTML += '<button type="button" class="btn btn-outline-primary" style="margin-top: 10px; width: 125px">Primary</button>';
                        infoLeftHTML += '</div>';
                        infoLeftHTML += '</div>';
                        infoLeftHTML += '<input type="checkbox" id="checkbox-' + index + '" class="checkbox" onclick="ChoosePrint(' + id + ')" />';
                        infoLeftHTML += '</div>';

                        infoLeftHTML += '</div>';

                        $('#qr').append(infoLeftHTML);

                    });
                }


            },
            error: function (error) {

            }
        });
    });
    $(document).ready(function () {



        $.ajax({
            url: '/api/dashboard/total-assets',
            type: 'GET',
            dataType: 'json',
            success: function(data) {
                $.each(data, function(index, item) {
                    var id = item.id;
                    var infoLeftHTML = '<div class="row card" style="border: 2px solid #1a1d20; height: 190px; margin-bottom: 20px">';
                    infoLeftHTML += '<div class="col-md-6">';
                    infoLeftHTML += '<strong>' + item.name + '</strong>';
                    infoLeftHTML += '<p>Asset Code: ' + item.asset_code + '</p>';
                    infoLeftHTML += '</div>';
                    infoLeftHTML += '<div class="d-flex justify-content-end col-md-6">';
                    infoLeftHTML += '<div class="row">';
                    infoLeftHTML += '<img id="qr-code-' + index + '" src="http://quocphi-intern.loc/api/asset/qr-code/' + id + '" ' +
                        'style="margin-top:20px; width: 300px; height: 100px;">';
                    infoLeftHTML += '<div class="row">';
                    infoLeftHTML += '<button type="button" class="btn btn-outline-primary" style="margin-top: 10px; width: 125px">Primary</button>';
                    infoLeftHTML += '</div>';
                    infoLeftHTML += '</div>';
                    infoLeftHTML += '<input type="checkbox" id="checkbox-' + index + '" class="checkbox" onclick="ChoosePrint(' + id + ')" />';
                    infoLeftHTML += '</div>';

                    infoLeftHTML += '</div>';

                    $('#qr').append(infoLeftHTML);

                });

            },
            error: function(error) {
                console.log('Lỗi trong quá trình tải dữ liệu.');
            }
        });

        var currentPage = getUrlParameter('page') || 1;

        // Lấy thẻ chứa liên kết trang
        var paginationContainer = $('#pagination-links');
        function changePage(newPage) {
            var apiUrl = '/api/locations?page=' + newPage;
            $.ajax({
                url: apiUrl,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    var table = $('.location_table tbody');
                    table.empty();
                    $.each(data.data, function (index, item) {
                        var row = '<tr data-index="' + index + '">' +
                            '<th scope="row">' + (index + 1) + '</th>' +
                            '<td>' + item.name + '</td>' +
                            '<td>' + item.department.name + '</td>' +
                            '<td>' + item.note + '</td>' +
                            '<td>' +
                            '<button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="openCopyModal(' + item.id + ')"> <i class="bx bxs-copy-alt"></i> </button>' +
                            '<button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="openEditModal(' + item.id + ')"> <i class="bx bxs-edit"></i> </button>' +
                            '<button class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="openDeleteModal(' + item.id + ')"> <i class="ri-chat-delete-fill"></i> </button>' +
                            '</td>'
                        '</tr>';
                        table.append(row);
                    });

                    var paginationContainer = $('#pagination-links');


                    paginationContainer.empty();


                    var lastPage = data.last_page;


                    if (data.current_page > 1) {
                        var previousLink = '<li class="page-item"><a class="page-link" href="' + (data.current_page - 1) + '">&laquo; Previous</a></li>';
                        paginationContainer.append(previousLink);
                    }

                    for (var i = 1; i <= lastPage; i++) {
                        if (
                            i === 1 || // First page
                            i === data.current_page - 1 || // One page before the current page
                            i === data.current_page || // The current page
                            i === data.current_page + 1 || // One page after the current page
                            i === lastPage // Last page
                        ) {
                            var activeClass = (data.current_page === i) ? 'active' : '';
                            var pageLink = '<li class="page-item ' + activeClass + '"><a class="page-link" href="' + data.path + '?page=' + i + '">' + i + '</a></li>';
                            paginationContainer.append(pageLink);
                        } else if (
                            i === 2 && data.current_page > 4 || // After the first page, add an ellipsis if the current page is greater than 4
                            i === data.current_page - 2 && data.current_page > 5 || // Two pages before the current page
                            i === data.current_page + 2 && data.current_page < lastPage - 4 || // Two pages after the current page
                            i === lastPage - 1 // One page before the last page
                        ) {
                            var ellipsisLink = '<li class="page-item disabled"><span class="page-link">...</span></li>';
                            paginationContainer.append(ellipsisLink);
                        }
                    }
                    if (data.current_page < lastPage) {
                        var nextLink = '<li class="page-item"><a class="page-link" href="' + (data.current_page+1) + '">Next &raquo;</a></li>';
                        paginationContainer.append(nextLink);
                    }
                }
            });

        }
        paginationContainer.on('click', 'a.page-link', function(e) {
            e.preventDefault();
            var newPage = $(this).text();
            changePage(newPage);
        });
        changePage(currentPage);
// --------------------

        var currentPage_asset = getUrlParameter('page') || 1;
        var paginationContainer_asset = $('#pagination-links-asset');
        function changePage_asset(newPage) {
            var apiUrl = '/api/asset?page=' + newPage;
            $.ajax({
                url: apiUrl,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    var table = $('.asset_table tbody');
                    table.empty();
                    $.each(data.data.data, function (index, item) {
                        var row = '<tr data-index="' + index + '">' +
                            '<th scope="row">' + (index + 1) + '</th>' +
                            '<td>' + item.asset_code + '</td>' +
                            '<td>' + item.name + '</td>' +
                            '<td>' + item.condition.name + '</td>' +
                            '<td>' +
                            // '<button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="openCopyModal(' + item.id + ')"> <i class="bx bxs-copy-alt"></i> </button>' +
                            // '<button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="openEditModal(' + item.id + ')"> <i class="bx bxs-edit"></i> </button>' +
                            // '<button class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="openDeleteModal(' + item.id + ')"> <i class="ri-chat-delete-fill"></i> </button>' +
                            '</td>'+
                            '<td></td>' +
                            '<td>' + item.note + '</td>' +
                        '</tr>';
                        table.append(row);
                    });

                    var paginationContainer_asset = $('#pagination-links-asset');


                    paginationContainer_asset.empty();


                    var lastPage = data.data.last_page;


                    if (data.data.current_page > 1) {
                        var previousLink = '<li class="page-item"><a class="page-link" href="' + (data.data.current_page - 1) + '">&laquo; Previous</a></li>';
                        paginationContainer_asset.append(previousLink);
                    }

                    for (var i = 1; i <= lastPage; i++) {
                        if (
                            i === 1 || // First page
                            i === data.data.current_page - 1 || // One page before the current page
                            i === data.data.current_page || // The current page
                            i === data.data.current_page + 1 || // One page after the current page
                            i === lastPage // Last page
                        ) {
                            var activeClass = (data.data.current_page === i) ? 'active' : '';
                            var pageLink = '<li class="page-item ' + activeClass + '"><a class="page-link" href="' + data.data.path + '?page=' + i + '">' + i + '</a></li>';
                            paginationContainer_asset.append(pageLink);
                        } else if (
                            i === 2 && data.data.current_page > 4 || // After the first page, add an ellipsis if the current page is greater than 4
                            i === data.data.current_page - 2 && data.data.current_page > 5 || // Two pages before the current page
                            i === data.data.current_page + 2 && data.data.current_page < lastPage - 4 || // Two pages after the current page
                            i === data.data.lastPage - 1 // One page before the last page
                        ) {
                            var ellipsisLink = '<li class="page-item disabled"><span class="page-link">...</span></li>';
                            paginationContainer_asset.append(ellipsisLink);
                        }
                    }
                    if (data.data.current_page < lastPage) {
                        var nextLink = '<li class="page-item"><a class="page-link" href="' + (data.data.current_page+1) + '">Next &raquo;</a></li>';
                        paginationContainer_asset.append(nextLink);
                    }
                }
            });

        }
        paginationContainer_asset.on('click', 'a.page-link', function(e) {
            e.preventDefault();
            var newPage = $(this).text();
            changePage_asset(newPage);
        });
        changePage_asset(currentPage_asset);
// ----------------

        $('#department').change(function () {
            var selectedDepartment = $(this).val();
            if (selectedDepartment !== '') {
                $('#floor_div').show();
                $('#unit_div').show();
                $('#building_div').show();
                $('#street_div').show();
                $('#city_div').show();
                $('#state_div').show();
                $('#country_div').show();
                $('#zipcode_div').show();
            } else {
                $('#floor_div').hide();
                $('#unit_div').hide();
                $('#building_div').hide();
                $('#street_div').hide();
                $('#city_div').hide();
                $('#state_div').hide();
                $('#country_div').hide();
                $('#zipcode_div').hide();
            }


            });
        $('#department_create').change(function () {
            var selectedDepartment = $(this).val();
            if (selectedDepartment !== '') {
                    $('#floor_div_create').show();
                    $('#unit_div_create').show();
                    $('#building_div_create').show();
                    $('#street_div_create').show();
                    $('#city_div_create').show();
                    $('#state_div_create').show();
                    $('#country_div_create').show();
                    $('#zipcode_div_create').show();
                $('#floor, #unit_create, #building_create, #street_address_create, #city_create, #state_create, #country_name_create, #zipcode_create').prop('disabled', false);
            } else {
                $('#floor_div_create').hide();
                $('#unit_div_create').hide();
                $('#building_div_create').hide();
                $('#street_div_create').hide();
                $('#city_div_create').hide();
                $('#state_div_create').hide();
                $('#country_div_create').hide();
                $('#zipcode_div_create').hide();
                $('#floor_create, #unit_create, #building_create, #street_address_create, #city_create, #state_create, #country_name_create, #zipcode_create').prop('disabled', true);
            }
        });

    });

    function getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, ' '));
    }
})();

