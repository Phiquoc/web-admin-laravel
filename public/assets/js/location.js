$(document).ready(function () {
    $("#save_asset").click(function (){
        var assetName = $('#asset_name_create').val();
        var assetCategory = $('#asset_category_create').val();
        var assetLocation = $('#asset_location_create').val();
        var assetNote = $('#asset_note_create').val();
        var assetCondition = $('#asset_condition_create').val();
        var assetCountry = $('#asset_country_create').val();
        // var assetManufactory = $('#asset_manufactory_create').val();
        // var assetModel = $('#asset_model_create').val();
        // var assetPrice = $('#asset_price_create').val();
        // var assetSupplier = $('#asset_supplier_create').val();
        var csrfToken = $('meta[name="csrf-token"]').attr('content');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': csrfToken
            }
        });
        var data = {
            name : assetName,
            condition_id : assetCondition,
            note : assetNote,
            category_id : assetCategory,
            country_id : assetCountry,
            location_id : assetLocation,

        };

        $.ajax({
            type: "POST",
            url: `/api/asset`,
            data: data,
            success: function (response) {

                Swal.fire({
                    icon: 'success',
                    title: 'Success!',
                    text: 'Request was successful!',
                    showConfirmButton: true,
                    timer: 2000,
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.reload();
                    }
                });

            },
            error: function (error) {
                console.error("Request failed:", error);
            }
        });

    });
    $("#Chang_location").click(function (){

        var id = $("#id_edit_location").val();
        var name = $("#location_name").val();
        var note = $('#note').val();
        var department_id =  $('#department').val();
        var data = {
            name: name,
            note: note,
            department_id: department_id
        };
        var csrfToken = $('meta[name="csrf-token"]').attr('content');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': csrfToken
            }
        });
        $.ajax({
            type: "PUT",
            url: `/api/locations/${id}`,
            data: data,
            success: function (response) {
                Swal.fire({
                    icon: 'success',
                    title: 'Success!',
                    text: 'Request was successful!',
                    showConfirmButton: true,
                    timer: 2000,
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.reload();
                    }
                });

            },
            error: function (error) {
                console.error("Request failed:", error);
            }
        });

    });
    $("#save_location").click(function () {

        var locationName = $("#location_name_create").val();
        var note = $("#note_create").val();
        var department = $("#department_create").val();
        var csrfToken = $('meta[name="csrf-token"]').attr('content');
        var dataToSend = {
            name: locationName,
            note: note,
            department_id: department
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': csrfToken
            }
        });

        $.ajax({
            type: "POST",
            url: "/locations/create",
            data: dataToSend,
            success: function (response) {
                Swal.fire({
                    icon: 'success',
                    title: 'Success!',
                    text: 'Request was successful!',
                    showConfirmButton: true,
                    timer: 2000,
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.reload();
                    }
                });

            },
            error: function (error) {
                console.error("Request failed:", error);
            }
        });
    });
    $("#confirmDelete").click(function() {

        var csrfToken = $('meta[name="csrf-token"]').attr('content');
        var id = $('#id_delete').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': csrfToken
            }
        });
        $.ajax({
            type: "POST",
            url: "/locations/delete/" + id,
            success: function (res) {
                console.log(res);
                Swal.fire({
                    icon: 'success',
                    title: 'Success!',
                    text: 'Request was successful!',

                    showConfirmButton: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.reload();
                    }
                });
            },
            error: function (error) {
                console.error("Request failed:", error);
            }
        });
    });
});
function openCopyModal(id){
    $.ajax({
        url: `/api/locations/copy/${id}`,
        type: 'POST',
        success: function(res) {
            Swal.fire({
                icon: 'success',
                title: 'Success!',
                text: 'Request was successful!',

                showConfirmButton: true
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.reload();
                }
            });
        }
    })
}
function openEditModal(id) {
    $.ajax({
        url: `/locations/get/${id}`,
        type: 'GET',
        success: function(res) {

            const department_id = res.department.id;
            $('#location_name').val(res['name']);
            $('#department').val(department_id);
            $('#id_edit_location').val(id);
            $('#note').val(res['note']);
            if(department_id!=='')
            {
                $('#floor_div').show();
                $('#unit_div').show();
                $('#building_div').show();
                $('#street_div').show();
                $('#city_div').show();
                $('#state_div').show();
                $('#country_div').show();
                $('#zipcode_div').show();
            }

            $('#edit_modal').modal('show');

        }
    })
}
function openImportModal(){
    $('#importModal').modal('show');
}
function openCreateModal(){
    $('#create_modal').modal('show');

}
function openDeleteModal(id){
    $.ajax({
        url: `/locations/get/${id}`,
        type: 'GET',
        success: function(res) {
            console.log(res);
            $('#delete_modal').modal('show');
            $('#id_delete').val(res.id);
        }
    });

}
