var loginApiUrl = "/api/login";
var vueInstance =  new Vue({
        el: '#container',
        data: {
        message: 'Hello, Vue.js!',
        },
        methods:{
            submitForm() {
                const email = this.$refs.email.value;
                const password = this.$refs.password.value;

                const data = {
                    email: email,
                    password: password,
                };
                axios.post(loginApiUrl, data)
                .then(response => {
                    if (response.status === 200) {
                        const token = response.data.access_token;

                        Cookies.set('token', token, { expires: 7 });

                        console.log(response.data.access_token);

                    } else {
                        console.error('Login failed');
                    }
                    })
                    .catch(error => {
                        console.error('Error making API request:', error);
                    });
            },
            myFunction() {
                console.log(1) ;
                axios.get('http://quocphi-intern.loc/api/asset')
                    .then(response => {
                        console.log(response);
                    })
                    .catch(error => {
                        console.error('Error making API request:', error);
                    });
            },

        }
});
