const Vue = require('vue');
const VueRouter = require('vue-router');
const Home = require('./components/HomeComponent.vue');
const About = require('./components/CreateComponent.vue');

const routes = [
    { path: '/', component: Home },
    { path: '/about', component: About },
];

const router = new VueRouter({
    routes,
});

module.exports = router;
