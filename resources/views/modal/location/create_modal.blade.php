<!-- Create Location Modal -->
<div class="modal fade" id="create_modal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Create Location</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">
                <form class="row g-3">
                    <div class="form-group">
                        <label for="location_name">Location Name</label>
                        <input type="text" class="form-control" id="location_name_create" name="location_name_create" placeholder="Location Name">
                    </div>
                    <div class="form-group">
                        <label for="note">Note</label>
                        <input type="text" class="form-control" id="note_create" name="note_create" placeholder="Note">
                    </div>
                    <div class="form-group">
                        <label for="department_create">Department</label>
                        <select class="form-control" id="department_create" name="department_create">
                            <option value="">Please Choose Department</option>
                            @foreach($departments as $department)
                                <option value="{{$department->id}}">{{$department->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6" id="floor_div_create" style="display:none">
                        <input type="text" class="form-control" id="floor_create" placeholder="FLOOR">
                    </div>
                    <div class="col-md-6" id="unit_div_create" style="display:none">
                        <input type="text" class="form-control" id="unit_create" placeholder="UNIT">
                    </div>
                    <div class="col-md-6" id="building_div_create" style="display:none">
                        <input type="text" class="form-control" id="building_create" placeholder="Building">
                    </div>
                    <div class="col-md-6" id="street_div_create" style="display:none">
                        <input type="text" class="form-control" id="street_address_create" placeholder="Street Address">
                    </div>
                    <div class="col-md-6" id="city_div_create" style="display:none">
                        <input type="text" class="form-control" id="city_create" placeholder="City">
                    </div>
                    <div class="col-md-6" id="state_div_create" style="display:none">
                        <input type="text" class="form-control" id="state_create" placeholder="State">
                    </div>
                    <div class="col-md-6" id="country_div_create" style="display:none">
                        <input type="text" class="form-control" id="country_name_create" placeholder="Country Name">
                    </div>
                    <div class="col-md-6" id="zipcode_div_create" style="display:none">
                        <input type="text" class="form-control" id="zipcode_create" placeholder="Zipcode">
                    </div>

                </form>
            </div>

            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="save_location">Save Changes</button>
            </div>
        </div>
    </div>
</div>


