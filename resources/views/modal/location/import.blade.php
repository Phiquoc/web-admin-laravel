<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="importLocationModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="importLocationModalLabel">Import Location</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if (Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif
                <form method="POST" action="{{ route('importData') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="csv_file">Choose file CSV:</label>
                        <input type="file" id="csv_file" class="form-control-file @error('csv_file') is-invalid @enderror" name="csv_file" accept=".csv">
                        @error('csv_file')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <p>
                        Accept files (click to download sample)
                        <br>
                         + CSV
                        <br>
                         + Excel
                    </p>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">
                            Tải lên
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
