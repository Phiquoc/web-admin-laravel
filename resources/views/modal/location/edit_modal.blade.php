<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Modal Edit</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form class="row g-3">
                    <input type="hidden" name="id_edit_location" id="id_edit_location">
                    <div class="col-md-12">
                        <input type="text" class="form-control" id="location_name" placeholder="Location Name">
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="note" placeholder="Note">
                    </div>
                    <div class="col-md-6">
                        <select id="department" class="form-control">
                            <option value="">Please Choose Department</option>
                            @foreach($departments as $department)
                                <option value="{{$department->id}}">{{$department->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6" id="floor_div" style="display:none">
                        <input type="text" class="form-control" id="floor" placeholder="FLOOR">
                    </div>
                    <div class="col-md-6" id="unit_div" style="display:none">
                        <input type="text" class="form-control" id="unit" placeholder="UNIT">
                    </div>
                    <div class="col-md-6" id="building_div" style="display:none">
                        <input type="text" class="form-control" id="building" placeholder="Building">
                    </div>
                    <div class="col-md-6" id="street_div" style="display:none">
                        <input type="text" class="form-control" id="street_address" placeholder="Street Address">
                    </div>
                    <div class="col-md-6" id="city_div" style="display:none">
                        <input type="text" class="form-control" id="city" placeholder="City">
                    </div>
                    <div class="col-md-6" id="state_div" style="display:none">
                        <input type="text" class="form-control" id="state" placeholder="State">
                    </div>
                    <div class="col-md-6" id="country_div" style="display:none">
                        <input type="text" class="form-control" id="country_name" placeholder="Country Name">
                    </div>
                    <div class="col-md-6" id="zipcode_div" style="display:none">
                        <input type="text" class="form-control" id="zipcode" placeholder="Zipcode">
                    </div>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="Chang_location">Save Changes</button>
            </div>
        </div>
    </div>
</div>
