
<!-- Create Asset Modal -->
<div class="modal fade" id="create_modal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Create Asset</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">
                <form class="row g-3">
                    <div class="form-group col-6">
                        <label for="asset_name">Asset Name</label>
                        <input type="text" class="form-control" id="asset_name_create" name="asset_name_create" placeholder="Asset Name">
                    </div>
                    <div class="form-group col-6">
                        <label for="asset_category_create">Category</label>
                        <select class="form-control" id="asset_category_create" name="asset_category_create">
                            <option value="">Please Choose Category</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-6">
                        <label for="asset_location_create">Location</label>
                        <select class="form-control" id="asset_location_create" name="asset_location_create">
                            <option value="">Please Choose Location</option>
                            @foreach($locations as $location)
                                <option value="{{$location->id}}">{{$location->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-6">
                        <label for="asset_country_create">Location</label>
                        <select class="form-control" id="asset_country_create" name="asset_country_create">
                            <option value="">Please Choose Country</option>
                            @foreach($countries as $country)
                                <option value="{{$country->id}}">{{$country->country_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-6">
                        <label for="note">Note</label>
                        <textarea id="asset_note_create" class="form-control" name="asset_note_create" rows="4" cols="50" placeholder="Note ........"></textarea>
                    </div>
                    <div class="form-group col-6">
                        <label for="asset_condition_create">Condition</label>
                        <select class="form-control" id="asset_condition_create" name="asset_condition_create">
                            <option value="">Please Choose Condition</option>
                            @foreach($conditions as $condition)
                                <option value="{{$condition->id}}">{{$condition->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <h6 class="col-12"><strong><i> Purchase Information</i></strong></h6>
                    <div class="form-group col-6">
                        <label for="asset_manufactory_create" >Manufactory</label>
                        <select class="form-control" id="asset_manufactory_create" name="asset_manufactory_create">
                            <option value="">Please Choose Manufactory</option>
                            @foreach($manufactories as $manufactory)
                                <option value="{{$manufactory->id}}">{{$manufactory->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="model col-6" style="display: none">
                        <div class="form-group">
                            <label for="asset_model_create" >Model</label>
                            <select class="form-control" id="asset_model_create" name="asset_model_create">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="asset_price_create">Price</label>
                            <input type="number" class="form-control" name="asset_price_create" id="asset_price_create">
                        </div>
                        <div class="form-group">
                            <label for="asset_supplier_create" >Supplier</label>
                            <select class="form-control" id="asset_supplier_create" name="asset_supplier_create">
                                <option value="">Please Choose Supplier</option>
                                @foreach($suppliers as $supplier)
                                    <option value="{{$supplier->id}}">{{$supplier->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </form>

            </div>

            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="save_asset">Create</button>
            </div>
        </div>
    </div>
</div>



