@extends('layouts/admin')
@section('locations','collapsed')
@section('users','collapsed')
@section('assets','collapsed')
@section('content')
    <main id="main" class="main">

        <div class="pagetitle">
            <h1>Dashboard</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item ">Dashboard</li>
                </ol>
            </nav>
        </div>

        <section class="section dashboard">
            <div class="row">

                <div class="col-lg-12">
                    <div class="row">

                        <!-- Sales Card -->
                        <div class="col-xxl-6 col-md-6">
                            <div class="card info-card sales-card">



                                <div class="card-body">
                                    <h5 class="card-title">Assets <span>| Today</span></h5>

                                    <div class="d-flex align-items-center">
                                        <div
                                            class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                            <i class="bi bi-cart"></i>
                                        </div>
                                        <div class="ps-3">
                                            <h6>{{count($assets)}}</h6>
                                            <span class="text-success small pt-1 fw-bold">12%</span> <span
                                                class="text-muted small pt-2 ps-1">increase</span>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div><!-- End Sales Card -->

                        <!-- Revenue Card -->
                        <div class="col-xxl-6 col-md-6">
                            <div class="card info-card revenue-card">


                                <div class="card-body">
                                    <h5 class="card-title">User <span>| This Month</span></h5>

                                    <div class="d-flex align-items-center">
                                        <div
                                            class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                            <i class="bi bi-person-check-fill"></i>
                                        </div>
                                        <div class="ps-3">
                                            <h6>{{count($users)}}</h6>
                                            <span class="text-success small pt-1 fw-bold">8%</span> <span
                                                class="text-muted small pt-2 ps-1">increase</span>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div><!-- End Revenue Card -->





                        <!-- Recent User  -->
                        <div class="col-12">
                            <div class="card recent-sales overflow-auto">

                                <div class="filter">
                                    <a class="icon" href="/" data-bs-toggle="">
                                        <button type="button" class="btn btn-primary rounded-pill">View more</button>
                                    </a>

                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">User <span>| List</span></h5>

                                    <table class="table table-borderless datatable">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Customer</th>
                                                <th scope="col">Email</th>
                                                <th scope="col">Country</th>
                                                <th scope="col">Role</th>
                                                <th scope="col">Department</th>
                                                <th scope="col">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($users as $user)
                                            <tr>
                                                <th scope="row"><a href="#">{{ $user->id }}</a></th>
                                                <th scope="row">{{ $user->name }}</th>
                                                <td><a href="#" class="text-primary">{{ $user->email }}</a></td>
                                                <td>{{ $user->country ? $user->country->country_name : 'N/A' }}</td>
                                                <td>{{ $user->role ? $user->role->name : 'N/A' }}</td>
                                                <td>{{ $user->department ? $user->department->name : 'N/A' }}</td>
                                                <td><span class="badge @if ($user->status == 'active') bg-success @else bg-warning @endif">{{ $user->status }}</span></td>
                                            </tr>
                                        @endforeach


                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div><!-- End Recent User  -->

                        <!-- Recent Assets -->
                        <div class="col-12">
                            <div class="card recent-sales overflow-auto">

                                <div class="filter">
                                    <a class="icon" href="/" data-bs-toggle="">
                                        <button type="button" class="btn btn-primary rounded-pill">View more</button>
                                    </a>

                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">Assets <span>| List</span></h5>

                                    <table class="table table-borderless datatable">
                                        <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Asset name</th>
                                            <th scope="col">Condition</th>

                                            <th scope="col">Locationư</th>
                                            <th scope="col">Country Name</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($assets as $asset)
                                            <tr>
                                                <th scope="col"><a href="#">{{ $asset->id }}</a></th>
                                                <td>{{ $asset->name }}</td>
                                                <td><a href="#" class="text-primary">{{ $asset->condition  }}</a>
                                                </td>
                                                <th>{{$asset->locations->name}}</th>
                                                <td>{{ $asset->country->country_name }}
                                                </td>
                                            </tr>
                                        @endforeach


                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div><!-- End Recent Assets -->

                    </div>
                </div><!-- End Left side columns -->

                <!-- Right side columns -->


            </div>
        </section>

    </main>
@endsection
