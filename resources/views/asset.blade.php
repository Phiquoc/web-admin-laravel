@extends('layouts/admin')
@section('home','collapsed')
@section('users','collapsed')
@section('locations','collapsed')
@section('title',' Assets')
@section('content')
    <main id="main" class="main">

        <div class="pagetitle">
            <h1>Assets</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item ">Assets</li>
                </ol>
            </nav>
        </div>

        <section class="section profile">
            <div class="row">
                @if(Session::has('message'))
                    <div class="alert alert-success"> <!-- You can style this as needed -->
                        {{ Session::get('message') }}
                    </div>
                @endif
                @if(session('errors'))
                    <div class="alert alert-danger">
                        <ul>
                            @foreach(session('errors') as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                    <div class="card-body col-12">
                        <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">

                            <li class="nav-item col-6" role="presentation">
                                <button type="submit" class="nav-link active" data-bs-toggle="tab" data-bs-target="#asset" aria-selected="true" role="tab">Asset</button>
                            </li>

                            <li class="nav-item col-6" role="presentation">
                                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#Qrcode" aria-selected="false" role="tab" tabindex="-1">QR Code</button>
                            </li>



                        </ul>
                        <div class="tab-content pt-2">
                            <div class="tab-pane fade profile-overview active show" id="asset" role="tabpanel">
                                <div class="d-flex align-items-center">
                                    <input class="datatable-input asset_search col-6" placeholder="Search..." type="search" title="Search within table" style="border: none;">
                                    <div class="col-6 d-flex justify-content-end">
                                        <a class="btn btn-primary mx-2" type="button" data-toggle="tooltip"
                                           data-placement="top" title="" data-original-title="Edit"
                                           onclick="openImportModal()"> Import Data </a>
                                        <a class="btn btn-primary mx-2" type="button" data-toggle="tooltip"
                                           data-placement="top" title="" data-original-title="Edit"
                                           onclick="openCreateModal()"> New Asset </a>
                                    </div>
                                </div>


                                <div class="card-body">
                                    <h5 class="card-title">Asset <span>| List</span></h5>
                                    <table class="table table-borderless asset_table" id="location-table">
                                        <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Code</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Location</th>
                                            <th scope="col">Purchase</th>
                                            <th scope="col">Price</th>
                                            <th scope="col">Notes</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr id="location-template" style="display: none;">
                                            <td scope="row"><a href="#"></a></td>
                                            <td scope="row"></td>
                                            <td><a href="#" class="text-primary"></a></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <ul class="pagination pagination_asset" id="pagination-links-asset">

                                    </ul>


                                </div>

                            </div>

                            <div class="tab-pane fade profile-edit pt-3" id="Qrcode" role="tabpanel">
                                <input class="datatable-input asset_qr_search col-6" placeholder="Search..." type="search" title="Search within table"
                                       style="border: none; margin-bottom: 15px;">
                                <div id="qr">

                                </div>

                            </div>





                        </div>

                    </div>
            </div>
        </section>

    </main>
    <script>
        function ChoosePrint(id) {
            console.log('Checkbox ' + id + ' đã được chọn hoặc bỏ chọn.');
        }
    </script>

    @extends('modal.asset.create_modal')
@endsection
