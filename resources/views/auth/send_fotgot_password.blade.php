<!DOCTYPE html>
<html>
<head>
    <title>Reset Password</title>
</head>
<body>
<p>Hello {{ $user->name }},</p>
<p>Please reset your password by clicking the following link:</p>
<a href="{{ route('password.reset', ['token' => $token, 'email' => $user->email]) }}">Reset Password</a>
<p>If you did not request a password reset, please ignore this email.</p>
</body>
</html>
