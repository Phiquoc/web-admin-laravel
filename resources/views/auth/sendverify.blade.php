<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Account Activation</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body style="background-color: #f5f5f5; padding: 20px;">
<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card" style="border: 1px solid #ccc; box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.1);">
                <div class="card-header" style="background-color: #007bff; color: #fff; text-align: center;">
                    <h2>Account Activation</h2>
                </div>
                <div class="card-body">
                    <p>Hello {{ $username }},</p>
                    <p>Please use the following token to activate your account:</p>
                    <p><strong>{{ $token }}</strong></p>
                    <p>Thank you for using our service.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Tải JavaScript của Bootstrap từ một CDN (tuỳ chọn) -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.3/dist/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
