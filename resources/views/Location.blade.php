@extends('layouts/admin')
@section('home','collapsed')
@section('users','collapsed')
@section('assets','collapsed')
@section('title',' Location')
@section('content')
    <main id="main" class="main">

        <div class="pagetitle">
            <h1>Locations</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item ">Locations</li>
                </ol>
            </nav>
        </div>

        <section class="section profile">
            <div class="row">
                @if(Session::has('message'))
                    <div class="alert alert-success"> <!-- You can style this as needed -->
                        {{ Session::get('message') }}
                    </div>
                @endif
                @if(session('errors'))
                    <div class="alert alert-danger">
                        <ul>
                            @foreach(session('errors') as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                <div class="col-12">

                    <div class="card">
                        <div class="card-body pt-6">
                            <ul class="nav nav-tabs nav-tabs-bordered" role="tablist">

                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" data-bs-toggle="tab"
                                            data-bs-target="#profile-overview" aria-selected="true" role="tab">Location
                                    </button>
                                </li>

                            </ul>
                            <div class="tab-content pt-12">

                                <div class="tab-pane fade show active profile-overview" id="profile-overview"
                                     role="tabpanel">
                                    <div class="card-body">
                                        <h5 class="card-title">Location <span>| List</span></h5>
                                        <div>
                                            <a class="btn btn-primary" type="button" data-toggle="tooltip"
                                               data-placement="top" title="" data-original-title="Edit"
                                               onclick="openImportModal()"> Import Data </a>
                                            <a class="btn btn-primary" type="button" data-toggle="tooltip"
                                               data-placement="top" title="" data-original-title="Edit"
                                               onclick="openCreateModal()"> New Location </a>
                                        </div>
                                        <table class="table table-borderless location_table" id="location-table">
                                            <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Name</th>
                                                <th scope="col">Department</th>
                                                <th scope="col">Note</th>
                                                <th scope="col">Action</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr id="location-template" style="display: none;">
                                                <td scope="row"><a href="#"></a></td>
                                                <td scope="row"></td>
                                                <td><a href="#" class="text-primary"></a></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <ul class="pagination" id="pagination-links"></ul>

                                    </div>

                                </div>


                            </div><!-- End Bordered Tabs -->

                        </div>
                    </div>

                </div>
            </div>
        </section>

    </main>






    @extends('modal.location.create_modal')
    @extends('modal.location.import')
    @extends('modal.location.edit_modal')
    @extends('modal.location.delete_modal')
@endsection
